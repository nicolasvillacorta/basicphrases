package com.example.basicphrases;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public void play(View view){

        Button buttonPressed = (Button) view;


        Log.i("Button pressed", buttonPressed.getTag().toString());

        // El primer parametro del identifier, "buttonPressed,getTag().toString()" es el nombre del archivo que se va a reproducir. (Tiene que estar en minuscula).
        MediaPlayer mediaPlayer = MediaPlayer.create(this, getResources().getIdentifier(buttonPressed.getTag().toString(), "raw", getPackageName()));
        mediaPlayer.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
